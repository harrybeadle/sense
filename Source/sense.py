from matplotlib import pyplot as plot
import subprocess as sp
import curses as c
import numpy as np
from scipy.interpolate import spline

cmd = ["sensors", "-A"]


t = [1]
d = {}

fig = plot.figure()
fig.canvas.draw()


while True:
    p = sp.Popen(cmd, stdout=sp.PIPE)
    out, err = p.communicate()
    out = out.decode()
    sources = out.split("\n\n")
    for source in sources:
        s = source.split('\n')
        source_name = s[0]
        for line in s:
            if ':' in line:
                l = line.split(':')
                field = l[0].replace(' ', '-')
                value = l[1].split('(')[0]
                value = value.replace('+', '').replace('C', '').replace('°', '').replace('RPM', '')
                value = float(value)
                key = f"{source_name}/{field}"
                print(f"{key}: {value}")
                if key in d:                    
                    d[key].append(value)
                else:
                    if 'fan' not in key:
                        d[key] = [value]

    l = [k for k in d]
    l.sort()

    tsmooth = np.linspace(min(t), max(t), len(t) * 20)

    for k in l:
        if len(d[k]) > 3:
            ksmooth = spline(t, d[k], tsmooth)
            plot.plot(tsmooth, ksmooth)
    
    plot.legend(l)

    t.append(t[-1]+1)
    if t[-1] > 100:
        break
    plot.pause(0.001) 